## README
Repositório do site da empresa Pax-Gurgel
### Informações
O site foi desenvolvido utilizando:

 - Django Framework (1.11.4)
 - Python (3.6.2)
 - PostgreSQL
 
### Responsáveis
Yan Diniz (***Analista e Desenvolvedor de Software***)
Júlio Noronha (***Project Owner***)
	